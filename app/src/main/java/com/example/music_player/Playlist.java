package com.example.music_player;

import com.example.music_player.ui.main.allSongsFrag;

import java.util.ArrayList;

public class Playlist {
    private String title;
    private ArrayList<Song> songs;

    public ArrayList<Song> getSongs() {
        return songs;
    }

    public void setSongs(ArrayList<Song> songs) {
        this.songs = songs;
    }

    public Playlist(String name, ArrayList<Song> songs) {
        this.title = name;
        this.songs = songs;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public void addSong(Song song) {
        songs.add(song);
    }
    public void addSongByTitle(String title) {
        songs.add(allSongsFrag.getSongByTitle(title));
    }
}
