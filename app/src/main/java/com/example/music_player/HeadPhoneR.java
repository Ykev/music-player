package com.example.music_player;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class HeadPhoneR extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(intent.ACTION_HEADSET_PLUG)){
            int state = intent.getIntExtra("state", -1);
            switch (state){
                case 0://not connected
                    Toast.makeText(context.getApplicationContext(), "unplugged", Toast.LENGTH_SHORT).show();
                    break;
                case 1:
                    Toast.makeText(context.getApplicationContext(), "plugged", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    Toast.makeText(context.getApplicationContext(), "else", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
