package com.example.music_player.ui.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.ContextWrapper;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.music_player.R;

import java.io.File;

public class recordActivity extends AppCompatActivity {

    ImageView startRecordBtn;
    boolean doneRecording = false;
    boolean isRecording = false;
    MediaRecorder mediaRecorder;
    MediaPlayer mediaPlayer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);
        getMicrophonePermission();

        startRecordBtn = findViewById(R.id.start_record_button);

        startRecordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isRecording) { //stop action
                    mediaRecorder.stop();
                    mediaRecorder.release();
                    mediaRecorder=  null;

                    Toast.makeText(recordActivity.this, "Recording Stopped", Toast.LENGTH_SHORT).show();
                    startRecordBtn.setImageResource(R.drawable.ic_baseline_done);
                    doneRecording = true;
                    isRecording = false;
                }
                else if (!doneRecording) {//hasn't recorded yet | start action
                    try {
                        mediaRecorder = new MediaRecorder();
                        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                        mediaRecorder.setOutputFile(getRecordingFilePath());
                        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                        mediaRecorder.prepare();
                        mediaRecorder.start();

                        Toast.makeText(recordActivity.this, "Recording Started", Toast.LENGTH_SHORT).show();
                        isRecording = true;
                        startRecordBtn.setImageResource(R.drawable.ic_baseline_stop_24);
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                }
                else {//play recorded audio | done action
                    Uri uri = Uri.parse(getRecordingFilePath());
                    mediaPlayer = MediaPlayer.create(getApplicationContext(), uri);
                    mediaPlayer.start();
                    Toast.makeText(recordActivity.this, "Playing recoded audio", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    private void getMicrophonePermission(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[]
                            {Manifest.permission.RECORD_AUDIO}, 200);
        }
    }
    private  String getRecordingFilePath(){
        ContextWrapper contextWrapper = new ContextWrapper(getApplicationContext());
        File musicDirectory = contextWrapper.getExternalFilesDir(Environment.DIRECTORY_MUSIC);
        File file = new File(musicDirectory, "tempRecordingFile_Android_OVER" + ".mp3");
        return file.getPath();
    }
}