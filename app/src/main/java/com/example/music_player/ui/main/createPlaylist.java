package com.example.music_player.ui.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.music_player.Playlist;
import com.example.music_player.R;
import com.example.music_player.Song;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Arrays;

public class createPlaylist extends AppCompatActivity {

    FloatingActionButton doneCreatingBtn;
    EditText playlistName;
    ListView songsListView;
    SearchView searchView;
    ArrayList<String> chosenSongs = new ArrayList<String>();
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_playlist);
        playlistName = findViewById(R.id.playlistName);
        songsListView = findViewById(R.id.create_playlist_songsList);

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, Arrays.asList(allSongsFrag.namesList));
        songsListView.setAdapter(adapter);
        songsListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        songsListView.setItemsCanFocus(false);

        searchView = findViewById(R.id.create_playlist_search_song_bar);

        doneCreatingBtn = findViewById(R.id.done_creating_playlist_button);
        doneCreatingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newTitle = playlistName.getText().toString();
                if (newTitle.length() == 0) {
                    Toast.makeText(getApplicationContext(), "Misssing Title", Toast.LENGTH_SHORT).show();
                    return;
                } else if (Arrays.asList(playListsFrag.playlists_titles).contains(newTitle)) {
                    Toast.makeText(getApplicationContext(), "Title Already Taken", Toast.LENGTH_SHORT).show();
                    return;
                } else if (chosenSongs.size() == 0) {
                    Toast.makeText(getApplicationContext(), "No Songs Were Chosen", Toast.LENGTH_SHORT).show();
                    return;
                }

                Playlist newPlaylist = new Playlist(newTitle, new ArrayList<Song>());
                for (String i : chosenSongs) {
                    newPlaylist.addSongByTitle(i);
                }
                playListsFrag.addPlaylist(newPlaylist);
                createPlaylist.super.onBackPressed();
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        songsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String songTitle = adapterView.getItemAtPosition(i).toString();
                if(chosenSongs.contains(songTitle)){
                    chosenSongs.remove(songTitle);
                }else {
                    chosenSongs.add(songTitle);
                }
            }
        });
    }
}