package com.example.music_player.ui.main;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.music_player.Playlist;
import com.example.music_player.R;
import com.example.music_player.Song;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;


public class playListsFrag extends Fragment {

    private final String DATA_NAME = "playlists list";
    FloatingActionButton createNewPlaylistBtn;
    public static ArrayList<Playlist> playlists = null;
    public static String[] playlists_titles;
    public static ListView listView = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_play_lists, container, false);

        createNewPlaylistBtn = view.findViewById(R.id.create_playlist_button);
        listView = (ListView) view.findViewById(R.id.listViewPlaylists);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Playlist currentPlaylist = playlists.get(Arrays.asList(playlists_titles).indexOf(adapterView.getItemAtPosition(i)));
                startActivity(new Intent(getActivity().getApplicationContext(),playerActivity.class)
                        .putExtra("songsList", currentPlaylist.getSongs())
                        .putExtra("songIndex",  0));
            }
        });

        createNewPlaylistBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getActivity().getApplicationContext(),createPlaylist.class), 1);
            }
        });
        loadDataBase();
        updateListView();
        return view;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Toast.makeText(getActivity(), "saving data", Toast.LENGTH_SHORT).show();
        saveData();
    }
    public static void updateListView(){
        //syncs listView and playlists Arraylist
        playlists_titles = new  String[playlists.size()];
        for(int i = 0; i<playlists.size();i++) {
            playlists_titles[i] = playlists.get(i).getTitle();
        }
        listView.setAdapter(
                new ArrayAdapter<String>(listView.getContext(), android.R.layout.simple_list_item_1, playlists_titles));
    }
    public static void addPlaylist(Playlist playlist){
        playlists.add(playlist);
        updateListView();
    }
    private void saveData(){
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("shared preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(playlists);
        editor.putString(DATA_NAME, json);
        editor.apply();
    }
    private void loadDataBase(){
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("shared preferences", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(DATA_NAME, null);
        Type type = new TypeToken<ArrayList<Playlist>>() {}.getType();
        playlists = gson.fromJson(json, type);

        if(playlists == null)
            playlists = new ArrayList<>();
    }
}